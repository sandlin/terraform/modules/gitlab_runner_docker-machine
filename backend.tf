# Store state in the project.
terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/_____/terraform/state/groot"
    lock_address = "https://gitlab.com/api/v4/projects/_____/terraform/state/groot/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/_____/terraform/state/groot/lock"
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_min = 5
  }
}
