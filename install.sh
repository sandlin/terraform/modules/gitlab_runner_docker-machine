#!/bin/bash -x
if [[ ! -f "$HOME/.gitlab/tf_token.tfvars" ]]
then
  echo "Cannot find .tfvars in " + pwd
  return 1
fi

[ -f "./.gitlab_creds" ] && . ./.gitlab_creds

# DEBUG OPTION
#export TF_LOG=TRACE

IP=`curl ifconfig.me`
PLAN_FILE=myplan.tfplan
PLAN_JSON=myplan.json
rm $PLAN_FILE $PLAN_JSON
#export TF_VAR_service_account_file="/Users/jsandlin/.gcp/glrunner-sa.json"
terraform init -backend-config=$HOME/.gitlab/.creds
rm -f $PLAN_FILE $PLAN_JSON
terraform plan -var-file="$HOME/.gitlab/tf_token.tfvars" -out $PLAN_FILE
terraform show -json $PLAN_FILE > $PLAN_JSON
if [[ "$1" == "create" ]]
then
  terraform apply -var-file="$HOME/.gitlab/tf_token.tfvars"  --auto-approve
elif [[ "$1" == "destroy" ]]
then
  terraform destroy -var-file="$HOME/.gitlab/tf_token.tfvars" --auto-approve
fi