provider "google" {
    credentials = (var.service_account_json != "" ? var.service_account_json : file(pathexpand(var.service_account_file)))
    project = var.gcp_project
}