# PROJECT NAME

![Project Stage][project-stage-shield]
![Maintenance][maintenance-shield]
![Pipeline Status][pipeline-shield]


[![License][license-shield]](LICENSE.md)
[![Maintaner][maintainer-shield]](https://gitlab.com/jsandlin)

## Table of Contents
[[_TOC_]]

## Usage
This project is a TF module for creating docker-machine GitLab runners on GCP, then linking them to your GitLab instance.


You will create a `terraform.tfvars`. An example may look like the following. NOTE: Do not put your `runner_registration_token` in version control without encrypting in some way.
```
gitlab_url = "https://31.22.52.1"
gcp_project = "greenscar"
gcp_region = "us-east1"
gcp_zone = "us-east1-a"
manager_owner = "greenscar"
resource_prefix = "glrunner"

service_account_file = "/Users/greenscar/.gcp/glrunner-sa.json"
runner_registration_token = "xxxxxxxxxxxxxx"
# TOKEN obtained from http://34.145.52.149/admin/workers
```

####

<!-- Let's define some variables for ease of use. -->
[PROJECT_SUBURL]: sandlin/terraform/modules/gitlab_runner_docker-machine
[project-stage-shield]: https://img.shields.io/badge/project%20stage-development-yellowgreen.svg
[issue]: https://gitlab.com/[PROJECT_SUBURL]/issues
[license-shield]: https://img.shields.io/github/license/hassio-addons/repository-beta.svg
[maintenance-shield]: https://img.shields.io/maintenance/yes/2021.svg
[maintainer-shield]: https://img.shields.io/badge/maintainer-James_Sandlin-blueviolet
[pipeline-shield]: https://gitlab.com/[PROJECT_SUBURL]/groot/pipeline.svg
